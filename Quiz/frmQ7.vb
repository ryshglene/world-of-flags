﻿Public Class frmQ7

    Dim quizImage As String = frmQuestionMenu.quizImagesPath + frmQuestionMenu.quizImagesShuffled(7) + frmQuestionMenu.quizImagesExt
    Dim quizChoices() As String = {frmQuestionMenu.quizImagesShuffled(174), frmQuestionMenu.quizImagesShuffled(173), frmQuestionMenu.quizImagesShuffled(7), frmQuestionMenu.quizImagesShuffled(172)}
    Dim quizChoicesCorrectAnswer As String = quizChoices(2) ' Do not change this
    Dim quizChoicesShuffled = quizChoices.OrderBy(Function() frmQuestionMenu.randomNumberGenerator.Next).ToArray

    Private Sub frmQ7_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        imgQ7_MainImage.Image = Image.FromFile(quizImage)
        If frmOptions.quizSettingsCheatMode = True Then
            lblQ7_Choice0.Text = quizChoices(0)
            lblQ7_Choice1.Text = quizChoices(1)
            lblQ7_Choice2.Text = quizChoices(2)
            lblQ7_Choice3.Text = quizChoices(3)
            rbtnQ7_Choice2.Checked = True
        Else
            lblQ7_Choice0.Text = quizChoicesShuffled(0)
            lblQ7_Choice1.Text = quizChoicesShuffled(1)
            lblQ7_Choice2.Text = quizChoicesShuffled(2)
            lblQ7_Choice3.Text = quizChoicesShuffled(3)
        End If
    End Sub

    Private Sub btnQ7_OK_Click(sender As System.Object, e As System.EventArgs) Handles btnQ7_OK.Click
        Dim quizChoicesShuffledCorrectAnswerIndex As Integer = Array.IndexOf(quizChoicesShuffled, quizChoicesCorrectAnswer)
        If frmOptions.quizSettingsCheatMode = True Then
            quizChoicesShuffledCorrectAnswerIndex = Array.IndexOf(quizChoices, quizChoicesCorrectAnswer)
        Else
            quizChoicesShuffledCorrectAnswerIndex = Array.IndexOf(quizChoicesShuffled, quizChoicesCorrectAnswer)
        End If

        Dim quizChoicesShuffledDict As New Dictionary(Of String, Boolean)
        quizChoicesShuffledDict.Add("choice0", rbtnQ7_Choice0.Checked)
        quizChoicesShuffledDict.Add("choice1", rbtnQ7_Choice1.Checked)
        quizChoicesShuffledDict.Add("choice2", rbtnQ7_Choice2.Checked)
        quizChoicesShuffledDict.Add("choice3", rbtnQ7_Choice3.Checked)
        If quizChoicesShuffledDict("choice" & quizChoicesShuffledCorrectAnswerIndex) = True Then
            MsgBox("Correct!")
            frmQuestionMenu.quizScoreCounter = frmQuestionMenu.quizScoreCounter + 1
        Else
            MsgBox("Wrong! The correct answer is " & quizChoicesCorrectAnswer.ToString & ".")
        End If
        frmQuestionMenu.btnQuestionMenu_Q08.Enabled = False
        frmQuestionMenu.quizOKCounter = frmQuestionMenu.quizOKCounter + 1
        If frmQuestionMenu.quizOKCounter = 10 Then
            frmScore.Show()
            frmQuestionMenu.Close()
        Else
            frmQuestionMenu.Show()
        End If
        Me.Close()
    End Sub

    Private Sub btnQ7_Cancel_Click(sender As System.Object, e As System.EventArgs) Handles btnQ7_Cancel.Click
        frmQuestionMenu.Show()
        Me.Hide()
    End Sub
End Class