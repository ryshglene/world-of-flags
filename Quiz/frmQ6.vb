﻿Public Class frmQ6

    Dim quizImage As String = frmQuestionMenu.quizImagesPath + frmQuestionMenu.quizImagesShuffled(6) + frmQuestionMenu.quizImagesExt
    Dim quizChoices() As String = {frmQuestionMenu.quizImagesShuffled(177), frmQuestionMenu.quizImagesShuffled(176), frmQuestionMenu.quizImagesShuffled(6), frmQuestionMenu.quizImagesShuffled(175)}
    Dim quizChoicesCorrectAnswer As String = quizChoices(2) ' Do not change this
    Dim quizChoicesShuffled = quizChoices.OrderBy(Function() frmQuestionMenu.randomNumberGenerator.Next).ToArray

    Private Sub frmQ6_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        imgQ6_MainImage.Image = Image.FromFile(quizImage)
        If frmOptions.quizSettingsCheatMode = True Then
            lblQ6_Choice0.Text = quizChoices(0)
            lblQ6_Choice1.Text = quizChoices(1)
            lblQ6_Choice2.Text = quizChoices(2)
            lblQ6_Choice3.Text = quizChoices(3)
            rbtnQ6_Choice2.Checked = True
        Else
            lblQ6_Choice0.Text = quizChoicesShuffled(0)
            lblQ6_Choice1.Text = quizChoicesShuffled(1)
            lblQ6_Choice2.Text = quizChoicesShuffled(2)
            lblQ6_Choice3.Text = quizChoicesShuffled(3)
        End If
    End Sub

    Private Sub btnQ6_OK_Click(sender As System.Object, e As System.EventArgs) Handles btnQ6_OK.Click
        Dim quizChoicesShuffledCorrectAnswerIndex As Integer = Array.IndexOf(quizChoicesShuffled, quizChoicesCorrectAnswer)
        If frmOptions.quizSettingsCheatMode = True Then
            quizChoicesShuffledCorrectAnswerIndex = Array.IndexOf(quizChoices, quizChoicesCorrectAnswer)
        Else
            quizChoicesShuffledCorrectAnswerIndex = Array.IndexOf(quizChoicesShuffled, quizChoicesCorrectAnswer)
        End If

        Dim quizChoicesShuffledDict As New Dictionary(Of String, Boolean)
        quizChoicesShuffledDict.Add("choice0", rbtnQ6_Choice0.Checked)
        quizChoicesShuffledDict.Add("choice1", rbtnQ6_Choice1.Checked)
        quizChoicesShuffledDict.Add("choice2", rbtnQ6_Choice2.Checked)
        quizChoicesShuffledDict.Add("choice3", rbtnQ6_Choice3.Checked)
        If quizChoicesShuffledDict("choice" & quizChoicesShuffledCorrectAnswerIndex) = True Then
            MsgBox("Correct!")
            frmQuestionMenu.quizScoreCounter = frmQuestionMenu.quizScoreCounter + 1
        Else
            MsgBox("Wrong! The correct answer is " & quizChoicesCorrectAnswer.ToString & ".")
        End If
        frmQuestionMenu.btnQuestionMenu_Q07.Enabled = False
        frmQuestionMenu.quizOKCounter = frmQuestionMenu.quizOKCounter + 1
        If frmQuestionMenu.quizOKCounter = 10 Then
            frmScore.Show()
            frmQuestionMenu.Close()
        Else
            frmQuestionMenu.Show()
        End If
        Me.Close()
    End Sub

    Private Sub btnQ6_Cancel_Click(sender As System.Object, e As System.EventArgs) Handles btnQ6_Cancel.Click
        frmQuestionMenu.Show()
        Me.Hide()
    End Sub
End Class