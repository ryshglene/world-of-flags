﻿Public Class frmWelcomeMenu

    Private Sub btnWelcomeMenu_About_Click(sender As System.Object, e As System.EventArgs) Handles btnWelcomeMenu_About.Click
        frmAbout.Show()
        Me.Close()
    End Sub

    Private Sub btnWelcomeMenu_Help_Click(sender As System.Object, e As System.EventArgs) Handles btnWelcomeMenu_Help.Click
        frmHelp.Show()
        Me.Close()
    End Sub

    Private Sub btnWelcomeMenu_Options_Click(sender As System.Object, e As System.EventArgs) Handles btnWelcomeMenu_Options.Click
        frmOptions.Show()
        Me.Close()
    End Sub
    Private Sub btnWelcomeMenu_Play_Click(sender As System.Object, e As System.EventArgs) Handles btnWelcomeMenu_Play.Click
        frmQuestionMenu.Show()
        Me.Close()
    End Sub

    Private Sub btnWelcomeMenu_Exit_Click(sender As System.Object, e As System.EventArgs) Handles btnWelcomeMenu_Exit.Click
        If Application.OpenForms().OfType(Of frmOptions).Any Then
            frmOptions.Close()
        End If
        Me.Close()
    End Sub

    Private Sub frmWelcomeMenu_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        If frmOptions.quizSettingsDisplayHScore = True Then
            btnWelcomeMenu_Exit.Location = New Point(39, 285)
            Dim quizScoreFilepath As String = frmQuestionMenu.quizImagesPath
            Dim quizScoreFilename As String = "highscore.txt"
            If Not System.IO.File.Exists(quizScoreFilepath + quizScoreFilename) Then
                System.IO.File.Create(quizScoreFilepath + quizScoreFilename).Dispose()
            End If
            Dim quizScoreFile As New System.IO.StreamReader(quizScoreFilepath + quizScoreFilename)
            lblWelcomeMenu_HScore.Text = "Highest Score: " + Val(quizScoreFile.ReadToEnd).ToString
            quizScoreFile.Close()
            lblWelcomeMenu_HScore.Visible = True
        Else
            lblWelcomeMenu_HScore.Visible = False
        End If
        If Not System.IO.Directory.Exists(frmQuestionMenu.quizImagesPath) Then
            btnWelcomeMenu_Play.Enabled = False
            Dim msg As String =
            MsgBox("Directory containing quiz images does not exist. See the README included with the installer.", MsgBoxStyle.Exclamation, "C:\assets : No such file or directory")
            Me.Close()
        End If
    End Sub
End Class