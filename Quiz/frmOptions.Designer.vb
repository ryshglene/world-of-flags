﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmOptions
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.chkOptions_CheatMode = New System.Windows.Forms.CheckBox()
        Me.chkOptions_DisplayHScore = New System.Windows.Forms.CheckBox()
        Me.chkOptions_AutoSaveScore = New System.Windows.Forms.CheckBox()
        Me.lblOptions_Toggle = New System.Windows.Forms.Label()
        Me.btnOptions_OK = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'chkOptions_CheatMode
        '
        Me.chkOptions_CheatMode.AutoSize = True
        Me.chkOptions_CheatMode.Font = New System.Drawing.Font("Lucida Sans", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkOptions_CheatMode.Location = New System.Drawing.Point(12, 51)
        Me.chkOptions_CheatMode.Name = "chkOptions_CheatMode"
        Me.chkOptions_CheatMode.Size = New System.Drawing.Size(91, 18)
        Me.chkOptions_CheatMode.TabIndex = 3
        Me.chkOptions_CheatMode.Text = "Cheat mode"
        Me.chkOptions_CheatMode.UseVisualStyleBackColor = True
        '
        'chkOptions_DisplayHScore
        '
        Me.chkOptions_DisplayHScore.AutoSize = True
        Me.chkOptions_DisplayHScore.Font = New System.Drawing.Font("Lucida Sans", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkOptions_DisplayHScore.Location = New System.Drawing.Point(12, 75)
        Me.chkOptions_DisplayHScore.Name = "chkOptions_DisplayHScore"
        Me.chkOptions_DisplayHScore.Size = New System.Drawing.Size(144, 18)
        Me.chkOptions_DisplayHScore.TabIndex = 4
        Me.chkOptions_DisplayHScore.Text = "Display highest score"
        Me.chkOptions_DisplayHScore.UseVisualStyleBackColor = True
        '
        'chkOptions_AutoSaveScore
        '
        Me.chkOptions_AutoSaveScore.AutoSize = True
        Me.chkOptions_AutoSaveScore.Font = New System.Drawing.Font("Lucida Sans", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkOptions_AutoSaveScore.Location = New System.Drawing.Point(12, 99)
        Me.chkOptions_AutoSaveScore.Name = "chkOptions_AutoSaveScore"
        Me.chkOptions_AutoSaveScore.Size = New System.Drawing.Size(93, 18)
        Me.chkOptions_AutoSaveScore.TabIndex = 5
        Me.chkOptions_AutoSaveScore.Text = "Save scores"
        Me.chkOptions_AutoSaveScore.UseVisualStyleBackColor = True
        '
        'lblOptions_Toggle
        '
        Me.lblOptions_Toggle.AutoSize = True
        Me.lblOptions_Toggle.Font = New System.Drawing.Font("Lucida Console", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOptions_Toggle.Location = New System.Drawing.Point(10, 22)
        Me.lblOptions_Toggle.Name = "lblOptions_Toggle"
        Me.lblOptions_Toggle.Size = New System.Drawing.Size(152, 12)
        Me.lblOptions_Toggle.TabIndex = 6
        Me.lblOptions_Toggle.Text = "Toggle game settings:"
        '
        'btnOptions_OK
        '
        Me.btnOptions_OK.Location = New System.Drawing.Point(122, 123)
        Me.btnOptions_OK.Name = "btnOptions_OK"
        Me.btnOptions_OK.Size = New System.Drawing.Size(50, 23)
        Me.btnOptions_OK.TabIndex = 7
        Me.btnOptions_OK.Text = "OK"
        Me.btnOptions_OK.UseVisualStyleBackColor = True
        '
        'frmOptions
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(184, 152)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnOptions_OK)
        Me.Controls.Add(Me.lblOptions_Toggle)
        Me.Controls.Add(Me.chkOptions_AutoSaveScore)
        Me.Controls.Add(Me.chkOptions_DisplayHScore)
        Me.Controls.Add(Me.chkOptions_CheatMode)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Name = "frmOptions"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Options"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents chkOptions_CheatMode As System.Windows.Forms.CheckBox
    Friend WithEvents chkOptions_DisplayHScore As System.Windows.Forms.CheckBox
    Friend WithEvents chkOptions_AutoSaveScore As System.Windows.Forms.CheckBox
    Friend WithEvents lblOptions_Toggle As System.Windows.Forms.Label
    Friend WithEvents btnOptions_OK As System.Windows.Forms.Button
End Class
