﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmScore
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblScore_totalScore = New System.Windows.Forms.Label()
        Me.lblScore_realScore = New System.Windows.Forms.Label()
        Me.lblScore_scoreMsg1 = New System.Windows.Forms.Label()
        Me.lblScore_outof = New System.Windows.Forms.Label()
        Me.lblScore_Congrats = New System.Windows.Forms.Label()
        Me.lblScore_questions = New System.Windows.Forms.Label()
        Me.lblScore_totalquestions = New System.Windows.Forms.Label()
        Me.lblScore_yougot = New System.Windows.Forms.Label()
        Me.lblScore_correctOrNot = New System.Windows.Forms.Label()
        Me.lblScore_Msg = New System.Windows.Forms.Label()
        Me.btnScore_Return = New System.Windows.Forms.Button()
        Me.btnScore_Quit = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'lblScore_totalScore
        '
        Me.lblScore_totalScore.AutoSize = True
        Me.lblScore_totalScore.Font = New System.Drawing.Font("Lucida Sans", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblScore_totalScore.ForeColor = System.Drawing.Color.SteelBlue
        Me.lblScore_totalScore.Location = New System.Drawing.Point(64, 195)
        Me.lblScore_totalScore.Name = "lblScore_totalScore"
        Me.lblScore_totalScore.Size = New System.Drawing.Size(28, 18)
        Me.lblScore_totalScore.TabIndex = 0
        Me.lblScore_totalScore.Text = "10"
        '
        'lblScore_realScore
        '
        Me.lblScore_realScore.AutoSize = True
        Me.lblScore_realScore.Font = New System.Drawing.Font("Lucida Sans", 72.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblScore_realScore.ForeColor = System.Drawing.Color.SteelBlue
        Me.lblScore_realScore.Location = New System.Drawing.Point(63, 50)
        Me.lblScore_realScore.Name = "lblScore_realScore"
        Me.lblScore_realScore.Size = New System.Drawing.Size(169, 109)
        Me.lblScore_realScore.TabIndex = 4
        Me.lblScore_realScore.Text = "10"
        '
        'lblScore_scoreMsg1
        '
        Me.lblScore_scoreMsg1.AutoSize = True
        Me.lblScore_scoreMsg1.Location = New System.Drawing.Point(93, 37)
        Me.lblScore_scoreMsg1.Name = "lblScore_scoreMsg1"
        Me.lblScore_scoreMsg1.Size = New System.Drawing.Size(94, 13)
        Me.lblScore_scoreMsg1.TabIndex = 5
        Me.lblScore_scoreMsg1.Text = "You got a score of"
        '
        'lblScore_outof
        '
        Me.lblScore_outof.AutoSize = True
        Me.lblScore_outof.Font = New System.Drawing.Font("Lucida Console", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblScore_outof.Location = New System.Drawing.Point(18, 200)
        Me.lblScore_outof.Name = "lblScore_outof"
        Me.lblScore_outof.Size = New System.Drawing.Size(47, 11)
        Me.lblScore_outof.TabIndex = 7
        Me.lblScore_outof.Text = "out of"
        '
        'lblScore_Congrats
        '
        Me.lblScore_Congrats.AutoSize = True
        Me.lblScore_Congrats.Font = New System.Drawing.Font("Century Gothic", 21.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblScore_Congrats.ForeColor = System.Drawing.Color.SeaGreen
        Me.lblScore_Congrats.Location = New System.Drawing.Point(21, 18)
        Me.lblScore_Congrats.Name = "lblScore_Congrats"
        Me.lblScore_Congrats.Size = New System.Drawing.Size(246, 36)
        Me.lblScore_Congrats.TabIndex = 8
        Me.lblScore_Congrats.Text = "Congratulations!"
        '
        'lblScore_questions
        '
        Me.lblScore_questions.AutoSize = True
        Me.lblScore_questions.Font = New System.Drawing.Font("Lucida Console", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblScore_questions.ForeColor = System.Drawing.Color.SeaGreen
        Me.lblScore_questions.Location = New System.Drawing.Point(88, 140)
        Me.lblScore_questions.Name = "lblScore_questions"
        Me.lblScore_questions.Size = New System.Drawing.Size(117, 19)
        Me.lblScore_questions.TabIndex = 9
        Me.lblScore_questions.Text = "questions"
        '
        'lblScore_totalquestions
        '
        Me.lblScore_totalquestions.AutoSize = True
        Me.lblScore_totalquestions.Font = New System.Drawing.Font("Lucida Console", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblScore_totalquestions.Location = New System.Drawing.Point(90, 199)
        Me.lblScore_totalquestions.Name = "lblScore_totalquestions"
        Me.lblScore_totalquestions.Size = New System.Drawing.Size(117, 11)
        Me.lblScore_totalquestions.TabIndex = 10
        Me.lblScore_totalquestions.Text = "total questions."
        '
        'lblScore_yougot
        '
        Me.lblScore_yougot.AutoSize = True
        Me.lblScore_yougot.Font = New System.Drawing.Font("Lucida Console", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblScore_yougot.Location = New System.Drawing.Point(38, 125)
        Me.lblScore_yougot.Name = "lblScore_yougot"
        Me.lblScore_yougot.Size = New System.Drawing.Size(54, 11)
        Me.lblScore_yougot.TabIndex = 11
        Me.lblScore_yougot.Text = "You got"
        '
        'lblScore_correctOrNot
        '
        Me.lblScore_correctOrNot.AutoSize = True
        Me.lblScore_correctOrNot.Font = New System.Drawing.Font("Lucida Console", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblScore_correctOrNot.Location = New System.Drawing.Point(196, 125)
        Me.lblScore_correctOrNot.Name = "lblScore_correctOrNot"
        Me.lblScore_correctOrNot.Size = New System.Drawing.Size(54, 11)
        Me.lblScore_correctOrNot.TabIndex = 12
        Me.lblScore_correctOrNot.Text = "correct"
        '
        'lblScore_Msg
        '
        Me.lblScore_Msg.AutoSize = True
        Me.lblScore_Msg.Font = New System.Drawing.Font("Comic Sans MS", 21.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblScore_Msg.Location = New System.Drawing.Point(12, 213)
        Me.lblScore_Msg.Name = "lblScore_Msg"
        Me.lblScore_Msg.Size = New System.Drawing.Size(147, 40)
        Me.lblScore_Msg.TabIndex = 13
        Me.lblScore_Msg.Text = "N o i c e ."
        '
        'btnScore_Return
        '
        Me.btnScore_Return.Location = New System.Drawing.Point(210, 256)
        Me.btnScore_Return.Name = "btnScore_Return"
        Me.btnScore_Return.Size = New System.Drawing.Size(68, 31)
        Me.btnScore_Return.TabIndex = 14
        Me.btnScore_Return.Text = "Main Menu"
        Me.btnScore_Return.UseVisualStyleBackColor = True
        '
        'btnScore_Quit
        '
        Me.btnScore_Quit.Location = New System.Drawing.Point(6, 256)
        Me.btnScore_Quit.Name = "btnScore_Quit"
        Me.btnScore_Quit.Size = New System.Drawing.Size(68, 31)
        Me.btnScore_Quit.TabIndex = 15
        Me.btnScore_Quit.Text = "Quit"
        Me.btnScore_Quit.UseVisualStyleBackColor = True
        '
        'frmScore
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 292)
        Me.Controls.Add(Me.btnScore_Quit)
        Me.Controls.Add(Me.btnScore_Return)
        Me.Controls.Add(Me.lblScore_Msg)
        Me.Controls.Add(Me.lblScore_correctOrNot)
        Me.Controls.Add(Me.lblScore_yougot)
        Me.Controls.Add(Me.lblScore_totalquestions)
        Me.Controls.Add(Me.lblScore_questions)
        Me.Controls.Add(Me.lblScore_Congrats)
        Me.Controls.Add(Me.lblScore_outof)
        Me.Controls.Add(Me.lblScore_scoreMsg1)
        Me.Controls.Add(Me.lblScore_totalScore)
        Me.Controls.Add(Me.lblScore_realScore)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Name = "frmScore"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Results"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblScore_totalScore As System.Windows.Forms.Label
    Friend WithEvents lblScore_realScore As System.Windows.Forms.Label
    Friend WithEvents lblScore_scoreMsg1 As System.Windows.Forms.Label
    Friend WithEvents lblScore_outof As System.Windows.Forms.Label
    Friend WithEvents lblScore_Congrats As System.Windows.Forms.Label
    Friend WithEvents lblScore_questions As System.Windows.Forms.Label
    Friend WithEvents lblScore_totalquestions As System.Windows.Forms.Label
    Friend WithEvents lblScore_yougot As System.Windows.Forms.Label
    Friend WithEvents lblScore_correctOrNot As System.Windows.Forms.Label
    Friend WithEvents lblScore_Msg As System.Windows.Forms.Label
    Friend WithEvents btnScore_Return As System.Windows.Forms.Button
    Friend WithEvents btnScore_Quit As System.Windows.Forms.Button
End Class
