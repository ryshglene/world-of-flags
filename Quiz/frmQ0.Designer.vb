﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmQ0
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.imgQ0_MainImage = New System.Windows.Forms.PictureBox()
        Me.btnQ0_OK = New System.Windows.Forms.Button()
        Me.rbtnQ0_Choice0 = New System.Windows.Forms.RadioButton()
        Me.rbtnQ0_Choice3 = New System.Windows.Forms.RadioButton()
        Me.rbtnQ0_Choice2 = New System.Windows.Forms.RadioButton()
        Me.rbtnQ0_Choice1 = New System.Windows.Forms.RadioButton()
        Me.lblQ0_Choice0 = New System.Windows.Forms.Label()
        Me.lblQ0_Choice1 = New System.Windows.Forms.Label()
        Me.lblQ0_Choice3 = New System.Windows.Forms.Label()
        Me.lblQ0_Choice2 = New System.Windows.Forms.Label()
        Me.btnQ0_Cancel = New System.Windows.Forms.Button()
        Me.lblQ0_MainQuestion = New System.Windows.Forms.Label()
        CType(Me.imgQ0_MainImage, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'imgQ0_MainImage
        '
        Me.imgQ0_MainImage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.imgQ0_MainImage.Location = New System.Drawing.Point(12, 12)
        Me.imgQ0_MainImage.Name = "imgQ0_MainImage"
        Me.imgQ0_MainImage.Size = New System.Drawing.Size(240, 180)
        Me.imgQ0_MainImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.imgQ0_MainImage.TabIndex = 0
        Me.imgQ0_MainImage.TabStop = False
        '
        'btnQ0_OK
        '
        Me.btnQ0_OK.Location = New System.Drawing.Point(192, 335)
        Me.btnQ0_OK.Name = "btnQ0_OK"
        Me.btnQ0_OK.Size = New System.Drawing.Size(60, 25)
        Me.btnQ0_OK.TabIndex = 1
        Me.btnQ0_OK.Text = "OK"
        Me.btnQ0_OK.UseVisualStyleBackColor = True
        '
        'rbtnQ0_Choice0
        '
        Me.rbtnQ0_Choice0.AutoSize = True
        Me.rbtnQ0_Choice0.Font = New System.Drawing.Font("Lucida Sans Typewriter", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtnQ0_Choice0.ForeColor = System.Drawing.Color.RoyalBlue
        Me.rbtnQ0_Choice0.Location = New System.Drawing.Point(32, 219)
        Me.rbtnQ0_Choice0.Name = "rbtnQ0_Choice0"
        Me.rbtnQ0_Choice0.Size = New System.Drawing.Size(36, 22)
        Me.rbtnQ0_Choice0.TabIndex = 2
        Me.rbtnQ0_Choice0.TabStop = True
        Me.rbtnQ0_Choice0.Text = "A"
        Me.rbtnQ0_Choice0.UseVisualStyleBackColor = True
        '
        'rbtnQ0_Choice3
        '
        Me.rbtnQ0_Choice3.AccessibleDescription = ""
        Me.rbtnQ0_Choice3.AutoSize = True
        Me.rbtnQ0_Choice3.Font = New System.Drawing.Font("Lucida Sans Typewriter", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtnQ0_Choice3.ForeColor = System.Drawing.Color.RoyalBlue
        Me.rbtnQ0_Choice3.Location = New System.Drawing.Point(32, 303)
        Me.rbtnQ0_Choice3.Name = "rbtnQ0_Choice3"
        Me.rbtnQ0_Choice3.Size = New System.Drawing.Size(36, 22)
        Me.rbtnQ0_Choice3.TabIndex = 3
        Me.rbtnQ0_Choice3.TabStop = True
        Me.rbtnQ0_Choice3.Text = "D"
        Me.rbtnQ0_Choice3.UseVisualStyleBackColor = True
        '
        'rbtnQ0_Choice2
        '
        Me.rbtnQ0_Choice2.AutoSize = True
        Me.rbtnQ0_Choice2.Font = New System.Drawing.Font("Lucida Sans Typewriter", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtnQ0_Choice2.ForeColor = System.Drawing.Color.RoyalBlue
        Me.rbtnQ0_Choice2.Location = New System.Drawing.Point(32, 275)
        Me.rbtnQ0_Choice2.Name = "rbtnQ0_Choice2"
        Me.rbtnQ0_Choice2.Size = New System.Drawing.Size(36, 22)
        Me.rbtnQ0_Choice2.TabIndex = 4
        Me.rbtnQ0_Choice2.TabStop = True
        Me.rbtnQ0_Choice2.Text = "C"
        Me.rbtnQ0_Choice2.UseVisualStyleBackColor = True
        '
        'rbtnQ0_Choice1
        '
        Me.rbtnQ0_Choice1.AutoSize = True
        Me.rbtnQ0_Choice1.Font = New System.Drawing.Font("Lucida Sans Typewriter", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtnQ0_Choice1.ForeColor = System.Drawing.Color.RoyalBlue
        Me.rbtnQ0_Choice1.Location = New System.Drawing.Point(32, 247)
        Me.rbtnQ0_Choice1.Name = "rbtnQ0_Choice1"
        Me.rbtnQ0_Choice1.Size = New System.Drawing.Size(36, 22)
        Me.rbtnQ0_Choice1.TabIndex = 5
        Me.rbtnQ0_Choice1.TabStop = True
        Me.rbtnQ0_Choice1.Text = "B"
        Me.rbtnQ0_Choice1.UseVisualStyleBackColor = True
        '
        'lblQ0_Choice0
        '
        Me.lblQ0_Choice0.AutoSize = True
        Me.lblQ0_Choice0.Location = New System.Drawing.Point(91, 224)
        Me.lblQ0_Choice0.Name = "lblQ0_Choice0"
        Me.lblQ0_Choice0.Size = New System.Drawing.Size(64, 13)
        Me.lblQ0_Choice0.TabIndex = 6
        Me.lblQ0_Choice0.Text = "<country A>"
        '
        'lblQ0_Choice1
        '
        Me.lblQ0_Choice1.AutoSize = True
        Me.lblQ0_Choice1.Location = New System.Drawing.Point(90, 252)
        Me.lblQ0_Choice1.Name = "lblQ0_Choice1"
        Me.lblQ0_Choice1.Size = New System.Drawing.Size(64, 13)
        Me.lblQ0_Choice1.TabIndex = 7
        Me.lblQ0_Choice1.Text = "<country B>"
        '
        'lblQ0_Choice3
        '
        Me.lblQ0_Choice3.AutoSize = True
        Me.lblQ0_Choice3.Location = New System.Drawing.Point(90, 308)
        Me.lblQ0_Choice3.Name = "lblQ0_Choice3"
        Me.lblQ0_Choice3.Size = New System.Drawing.Size(65, 13)
        Me.lblQ0_Choice3.TabIndex = 8
        Me.lblQ0_Choice3.Text = "<country D>"
        '
        'lblQ0_Choice2
        '
        Me.lblQ0_Choice2.AutoSize = True
        Me.lblQ0_Choice2.Location = New System.Drawing.Point(90, 279)
        Me.lblQ0_Choice2.Name = "lblQ0_Choice2"
        Me.lblQ0_Choice2.Size = New System.Drawing.Size(64, 13)
        Me.lblQ0_Choice2.TabIndex = 9
        Me.lblQ0_Choice2.Text = "<country C>"
        '
        'btnQ0_Cancel
        '
        Me.btnQ0_Cancel.Location = New System.Drawing.Point(126, 335)
        Me.btnQ0_Cancel.Name = "btnQ0_Cancel"
        Me.btnQ0_Cancel.Size = New System.Drawing.Size(60, 25)
        Me.btnQ0_Cancel.TabIndex = 10
        Me.btnQ0_Cancel.Text = "Cancel"
        Me.btnQ0_Cancel.UseVisualStyleBackColor = True
        '
        'lblQ0_MainQuestion
        '
        Me.lblQ0_MainQuestion.AutoSize = True
        Me.lblQ0_MainQuestion.Font = New System.Drawing.Font("Lucida Sans", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblQ0_MainQuestion.Location = New System.Drawing.Point(17, 198)
        Me.lblQ0_MainQuestion.Name = "lblQ0_MainQuestion"
        Me.lblQ0_MainQuestion.Size = New System.Drawing.Size(229, 15)
        Me.lblQ0_MainQuestion.TabIndex = 11
        Me.lblQ0_MainQuestion.Text = "Which country has this kind of flag?"
        '
        'frmQ0
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(264, 372)
        Me.ControlBox = False
        Me.Controls.Add(Me.lblQ0_MainQuestion)
        Me.Controls.Add(Me.btnQ0_Cancel)
        Me.Controls.Add(Me.lblQ0_Choice2)
        Me.Controls.Add(Me.lblQ0_Choice3)
        Me.Controls.Add(Me.lblQ0_Choice1)
        Me.Controls.Add(Me.lblQ0_Choice0)
        Me.Controls.Add(Me.rbtnQ0_Choice1)
        Me.Controls.Add(Me.rbtnQ0_Choice2)
        Me.Controls.Add(Me.rbtnQ0_Choice3)
        Me.Controls.Add(Me.rbtnQ0_Choice0)
        Me.Controls.Add(Me.btnQ0_OK)
        Me.Controls.Add(Me.imgQ0_MainImage)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Name = "frmQ0"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Question 1"
        CType(Me.imgQ0_MainImage, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents imgQ0_MainImage As System.Windows.Forms.PictureBox
    Friend WithEvents btnQ0_OK As System.Windows.Forms.Button
    Friend WithEvents rbtnQ0_Choice0 As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnQ0_Choice3 As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnQ0_Choice2 As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnQ0_Choice1 As System.Windows.Forms.RadioButton
    Friend WithEvents lblQ0_Choice0 As System.Windows.Forms.Label
    Friend WithEvents lblQ0_Choice1 As System.Windows.Forms.Label
    Friend WithEvents lblQ0_Choice3 As System.Windows.Forms.Label
    Friend WithEvents lblQ0_Choice2 As System.Windows.Forms.Label
    Friend WithEvents btnQ0_Cancel As System.Windows.Forms.Button
    Friend WithEvents lblQ0_MainQuestion As System.Windows.Forms.Label
End Class
