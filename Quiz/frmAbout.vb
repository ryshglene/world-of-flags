﻿Public Class frmAbout

    Private Sub btnAbout_License_Click(sender As System.Object, e As System.EventArgs) Handles btnAbout_License.Click
        frmLicense.ShowDialog()
    End Sub

    Private Sub btnAbout_Close_Click(sender As System.Object, e As System.EventArgs) Handles btnAbout_Close.Click
        frmWelcomeMenu.Show()
        Me.Close()
    End Sub
End Class