﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmWelcomeMenu
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblWelcomeMenu_World = New System.Windows.Forms.Label()
        Me.lblWelcomeMenu_Of = New System.Windows.Forms.Label()
        Me.lblWelcomeMenu_Flags = New System.Windows.Forms.Label()
        Me.btnWelcomeMenu_Play = New System.Windows.Forms.Button()
        Me.btnWelcomeMenu_Options = New System.Windows.Forms.Button()
        Me.btnWelcomeMenu_Help = New System.Windows.Forms.Button()
        Me.btnWelcomeMenu_About = New System.Windows.Forms.Button()
        Me.lblWelcomeMenu_HScore = New System.Windows.Forms.Label()
        Me.btnWelcomeMenu_Exit = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'lblWelcomeMenu_World
        '
        Me.lblWelcomeMenu_World.AutoSize = True
        Me.lblWelcomeMenu_World.BackColor = System.Drawing.Color.Transparent
        Me.lblWelcomeMenu_World.Font = New System.Drawing.Font("Lucida Sans", 48.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWelcomeMenu_World.ForeColor = System.Drawing.Color.SteelBlue
        Me.lblWelcomeMenu_World.Location = New System.Drawing.Point(6, 10)
        Me.lblWelcomeMenu_World.Name = "lblWelcomeMenu_World"
        Me.lblWelcomeMenu_World.Size = New System.Drawing.Size(217, 72)
        Me.lblWelcomeMenu_World.TabIndex = 0
        Me.lblWelcomeMenu_World.Text = "World"
        '
        'lblWelcomeMenu_Of
        '
        Me.lblWelcomeMenu_Of.AutoSize = True
        Me.lblWelcomeMenu_Of.Font = New System.Drawing.Font("Lucida Sans", 36.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWelcomeMenu_Of.ForeColor = System.Drawing.Color.SteelBlue
        Me.lblWelcomeMenu_Of.Location = New System.Drawing.Point(202, 48)
        Me.lblWelcomeMenu_Of.Name = "lblWelcomeMenu_Of"
        Me.lblWelcomeMenu_Of.Size = New System.Drawing.Size(75, 55)
        Me.lblWelcomeMenu_Of.TabIndex = 1
        Me.lblWelcomeMenu_Of.Text = "of"
        '
        'lblWelcomeMenu_Flags
        '
        Me.lblWelcomeMenu_Flags.AutoSize = True
        Me.lblWelcomeMenu_Flags.Font = New System.Drawing.Font("Lucida Sans", 72.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWelcomeMenu_Flags.ForeColor = System.Drawing.Color.SteelBlue
        Me.lblWelcomeMenu_Flags.Location = New System.Drawing.Point(-7, 72)
        Me.lblWelcomeMenu_Flags.Name = "lblWelcomeMenu_Flags"
        Me.lblWelcomeMenu_Flags.Size = New System.Drawing.Size(307, 109)
        Me.lblWelcomeMenu_Flags.TabIndex = 2
        Me.lblWelcomeMenu_Flags.Text = "Flags"
        '
        'btnWelcomeMenu_Play
        '
        Me.btnWelcomeMenu_Play.Location = New System.Drawing.Point(39, 203)
        Me.btnWelcomeMenu_Play.Name = "btnWelcomeMenu_Play"
        Me.btnWelcomeMenu_Play.Size = New System.Drawing.Size(100, 35)
        Me.btnWelcomeMenu_Play.TabIndex = 3
        Me.btnWelcomeMenu_Play.Text = "Play"
        Me.btnWelcomeMenu_Play.UseVisualStyleBackColor = True
        '
        'btnWelcomeMenu_Options
        '
        Me.btnWelcomeMenu_Options.Location = New System.Drawing.Point(145, 203)
        Me.btnWelcomeMenu_Options.Name = "btnWelcomeMenu_Options"
        Me.btnWelcomeMenu_Options.Size = New System.Drawing.Size(100, 35)
        Me.btnWelcomeMenu_Options.TabIndex = 4
        Me.btnWelcomeMenu_Options.Text = "Options"
        Me.btnWelcomeMenu_Options.UseVisualStyleBackColor = True
        '
        'btnWelcomeMenu_Help
        '
        Me.btnWelcomeMenu_Help.Location = New System.Drawing.Point(39, 244)
        Me.btnWelcomeMenu_Help.Name = "btnWelcomeMenu_Help"
        Me.btnWelcomeMenu_Help.Size = New System.Drawing.Size(100, 35)
        Me.btnWelcomeMenu_Help.TabIndex = 5
        Me.btnWelcomeMenu_Help.Text = "Help"
        Me.btnWelcomeMenu_Help.UseVisualStyleBackColor = True
        '
        'btnWelcomeMenu_About
        '
        Me.btnWelcomeMenu_About.Location = New System.Drawing.Point(145, 244)
        Me.btnWelcomeMenu_About.Name = "btnWelcomeMenu_About"
        Me.btnWelcomeMenu_About.Size = New System.Drawing.Size(100, 35)
        Me.btnWelcomeMenu_About.TabIndex = 6
        Me.btnWelcomeMenu_About.Text = "About"
        Me.btnWelcomeMenu_About.UseVisualStyleBackColor = True
        '
        'lblWelcomeMenu_HScore
        '
        Me.lblWelcomeMenu_HScore.AutoSize = True
        Me.lblWelcomeMenu_HScore.Location = New System.Drawing.Point(152, 296)
        Me.lblWelcomeMenu_HScore.Name = "lblWelcomeMenu_HScore"
        Me.lblWelcomeMenu_HScore.Size = New System.Drawing.Size(80, 13)
        Me.lblWelcomeMenu_HScore.TabIndex = 8
        Me.lblWelcomeMenu_HScore.Text = "Highest Score: "
        '
        'btnWelcomeMenu_Exit
        '
        Me.btnWelcomeMenu_Exit.Location = New System.Drawing.Point(90, 285)
        Me.btnWelcomeMenu_Exit.Name = "btnWelcomeMenu_Exit"
        Me.btnWelcomeMenu_Exit.Size = New System.Drawing.Size(100, 35)
        Me.btnWelcomeMenu_Exit.TabIndex = 7
        Me.btnWelcomeMenu_Exit.Text = "Exit"
        Me.btnWelcomeMenu_Exit.UseVisualStyleBackColor = True
        '
        'frmWelcomeMenu
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 351)
        Me.ControlBox = False
        Me.Controls.Add(Me.lblWelcomeMenu_HScore)
        Me.Controls.Add(Me.btnWelcomeMenu_Exit)
        Me.Controls.Add(Me.lblWelcomeMenu_Of)
        Me.Controls.Add(Me.lblWelcomeMenu_World)
        Me.Controls.Add(Me.btnWelcomeMenu_About)
        Me.Controls.Add(Me.btnWelcomeMenu_Help)
        Me.Controls.Add(Me.btnWelcomeMenu_Options)
        Me.Controls.Add(Me.btnWelcomeMenu_Play)
        Me.Controls.Add(Me.lblWelcomeMenu_Flags)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Name = "frmWelcomeMenu"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Welcome - World of Flags"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblWelcomeMenu_World As System.Windows.Forms.Label
    Friend WithEvents lblWelcomeMenu_Of As System.Windows.Forms.Label
    Friend WithEvents lblWelcomeMenu_Flags As System.Windows.Forms.Label
    Friend WithEvents btnWelcomeMenu_Play As System.Windows.Forms.Button
    Friend WithEvents btnWelcomeMenu_Options As System.Windows.Forms.Button
    Friend WithEvents btnWelcomeMenu_Help As System.Windows.Forms.Button
    Friend WithEvents btnWelcomeMenu_About As System.Windows.Forms.Button
    Friend WithEvents lblWelcomeMenu_HScore As System.Windows.Forms.Label
    Friend WithEvents btnWelcomeMenu_Exit As System.Windows.Forms.Button
End Class
