﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmQ4
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.imgQ4_MainImage = New System.Windows.Forms.PictureBox()
        Me.btnQ4_OK = New System.Windows.Forms.Button()
        Me.rbtnQ4_Choice0 = New System.Windows.Forms.RadioButton()
        Me.rbtnQ4_Choice3 = New System.Windows.Forms.RadioButton()
        Me.rbtnQ4_Choice2 = New System.Windows.Forms.RadioButton()
        Me.rbtnQ4_Choice1 = New System.Windows.Forms.RadioButton()
        Me.lblQ4_Choice0 = New System.Windows.Forms.Label()
        Me.lblQ4_Choice1 = New System.Windows.Forms.Label()
        Me.lblQ4_Choice3 = New System.Windows.Forms.Label()
        Me.lblQ4_Choice2 = New System.Windows.Forms.Label()
        Me.btnQ4_Cancel = New System.Windows.Forms.Button()
        Me.lblQ4_MainQuestion = New System.Windows.Forms.Label()
        CType(Me.imgQ4_MainImage, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'imgQ4_MainImage
        '
        Me.imgQ4_MainImage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.imgQ4_MainImage.Location = New System.Drawing.Point(12, 12)
        Me.imgQ4_MainImage.Name = "imgQ4_MainImage"
        Me.imgQ4_MainImage.Size = New System.Drawing.Size(240, 180)
        Me.imgQ4_MainImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.imgQ4_MainImage.TabIndex = 0
        Me.imgQ4_MainImage.TabStop = False
        '
        'btnQ4_OK
        '
        Me.btnQ4_OK.Location = New System.Drawing.Point(192, 335)
        Me.btnQ4_OK.Name = "btnQ4_OK"
        Me.btnQ4_OK.Size = New System.Drawing.Size(60, 25)
        Me.btnQ4_OK.TabIndex = 1
        Me.btnQ4_OK.Text = "OK"
        Me.btnQ4_OK.UseVisualStyleBackColor = True
        '
        'rbtnQ4_Choice0
        '
        Me.rbtnQ4_Choice0.AutoSize = True
        Me.rbtnQ4_Choice0.Font = New System.Drawing.Font("Lucida Sans Typewriter", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtnQ4_Choice0.ForeColor = System.Drawing.Color.RoyalBlue
        Me.rbtnQ4_Choice0.Location = New System.Drawing.Point(32, 219)
        Me.rbtnQ4_Choice0.Name = "rbtnQ4_Choice0"
        Me.rbtnQ4_Choice0.Size = New System.Drawing.Size(36, 22)
        Me.rbtnQ4_Choice0.TabIndex = 2
        Me.rbtnQ4_Choice0.TabStop = True
        Me.rbtnQ4_Choice0.Text = "A"
        Me.rbtnQ4_Choice0.UseVisualStyleBackColor = True
        '
        'rbtnQ4_Choice3
        '
        Me.rbtnQ4_Choice3.AccessibleDescription = ""
        Me.rbtnQ4_Choice3.AutoSize = True
        Me.rbtnQ4_Choice3.Font = New System.Drawing.Font("Lucida Sans Typewriter", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtnQ4_Choice3.ForeColor = System.Drawing.Color.RoyalBlue
        Me.rbtnQ4_Choice3.Location = New System.Drawing.Point(32, 303)
        Me.rbtnQ4_Choice3.Name = "rbtnQ4_Choice3"
        Me.rbtnQ4_Choice3.Size = New System.Drawing.Size(36, 22)
        Me.rbtnQ4_Choice3.TabIndex = 3
        Me.rbtnQ4_Choice3.TabStop = True
        Me.rbtnQ4_Choice3.Text = "D"
        Me.rbtnQ4_Choice3.UseVisualStyleBackColor = True
        '
        'rbtnQ4_Choice2
        '
        Me.rbtnQ4_Choice2.AutoSize = True
        Me.rbtnQ4_Choice2.Font = New System.Drawing.Font("Lucida Sans Typewriter", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtnQ4_Choice2.ForeColor = System.Drawing.Color.RoyalBlue
        Me.rbtnQ4_Choice2.Location = New System.Drawing.Point(32, 275)
        Me.rbtnQ4_Choice2.Name = "rbtnQ4_Choice2"
        Me.rbtnQ4_Choice2.Size = New System.Drawing.Size(36, 22)
        Me.rbtnQ4_Choice2.TabIndex = 4
        Me.rbtnQ4_Choice2.TabStop = True
        Me.rbtnQ4_Choice2.Text = "C"
        Me.rbtnQ4_Choice2.UseVisualStyleBackColor = True
        '
        'rbtnQ4_Choice1
        '
        Me.rbtnQ4_Choice1.AutoSize = True
        Me.rbtnQ4_Choice1.Font = New System.Drawing.Font("Lucida Sans Typewriter", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtnQ4_Choice1.ForeColor = System.Drawing.Color.RoyalBlue
        Me.rbtnQ4_Choice1.Location = New System.Drawing.Point(32, 247)
        Me.rbtnQ4_Choice1.Name = "rbtnQ4_Choice1"
        Me.rbtnQ4_Choice1.Size = New System.Drawing.Size(36, 22)
        Me.rbtnQ4_Choice1.TabIndex = 5
        Me.rbtnQ4_Choice1.TabStop = True
        Me.rbtnQ4_Choice1.Text = "B"
        Me.rbtnQ4_Choice1.UseVisualStyleBackColor = True
        '
        'lblQ4_Choice0
        '
        Me.lblQ4_Choice0.AutoSize = True
        Me.lblQ4_Choice0.Location = New System.Drawing.Point(91, 224)
        Me.lblQ4_Choice0.Name = "lblQ4_Choice0"
        Me.lblQ4_Choice0.Size = New System.Drawing.Size(64, 13)
        Me.lblQ4_Choice0.TabIndex = 6
        Me.lblQ4_Choice0.Text = "<country A>"
        '
        'lblQ4_Choice1
        '
        Me.lblQ4_Choice1.AutoSize = True
        Me.lblQ4_Choice1.Location = New System.Drawing.Point(90, 252)
        Me.lblQ4_Choice1.Name = "lblQ4_Choice1"
        Me.lblQ4_Choice1.Size = New System.Drawing.Size(64, 13)
        Me.lblQ4_Choice1.TabIndex = 7
        Me.lblQ4_Choice1.Text = "<country B>"
        '
        'lblQ4_Choice3
        '
        Me.lblQ4_Choice3.AutoSize = True
        Me.lblQ4_Choice3.Location = New System.Drawing.Point(90, 308)
        Me.lblQ4_Choice3.Name = "lblQ4_Choice3"
        Me.lblQ4_Choice3.Size = New System.Drawing.Size(65, 13)
        Me.lblQ4_Choice3.TabIndex = 8
        Me.lblQ4_Choice3.Text = "<country D>"
        '
        'lblQ4_Choice2
        '
        Me.lblQ4_Choice2.AutoSize = True
        Me.lblQ4_Choice2.Location = New System.Drawing.Point(90, 279)
        Me.lblQ4_Choice2.Name = "lblQ4_Choice2"
        Me.lblQ4_Choice2.Size = New System.Drawing.Size(64, 13)
        Me.lblQ4_Choice2.TabIndex = 9
        Me.lblQ4_Choice2.Text = "<country C>"
        '
        'btnQ4_Cancel
        '
        Me.btnQ4_Cancel.Location = New System.Drawing.Point(126, 335)
        Me.btnQ4_Cancel.Name = "btnQ4_Cancel"
        Me.btnQ4_Cancel.Size = New System.Drawing.Size(60, 25)
        Me.btnQ4_Cancel.TabIndex = 10
        Me.btnQ4_Cancel.Text = "Cancel"
        Me.btnQ4_Cancel.UseVisualStyleBackColor = True
        '
        'lblQ4_MainQuestion
        '
        Me.lblQ4_MainQuestion.AutoSize = True
        Me.lblQ4_MainQuestion.Font = New System.Drawing.Font("Lucida Sans", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblQ4_MainQuestion.Location = New System.Drawing.Point(17, 198)
        Me.lblQ4_MainQuestion.Name = "lblQ4_MainQuestion"
        Me.lblQ4_MainQuestion.Size = New System.Drawing.Size(229, 15)
        Me.lblQ4_MainQuestion.TabIndex = 11
        Me.lblQ4_MainQuestion.Text = "Which country has this kind of flag?"
        '
        'frmQ4
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(264, 372)
        Me.ControlBox = False
        Me.Controls.Add(Me.lblQ4_MainQuestion)
        Me.Controls.Add(Me.btnQ4_Cancel)
        Me.Controls.Add(Me.lblQ4_Choice2)
        Me.Controls.Add(Me.lblQ4_Choice3)
        Me.Controls.Add(Me.lblQ4_Choice1)
        Me.Controls.Add(Me.lblQ4_Choice0)
        Me.Controls.Add(Me.rbtnQ4_Choice1)
        Me.Controls.Add(Me.rbtnQ4_Choice2)
        Me.Controls.Add(Me.rbtnQ4_Choice3)
        Me.Controls.Add(Me.rbtnQ4_Choice0)
        Me.Controls.Add(Me.btnQ4_OK)
        Me.Controls.Add(Me.imgQ4_MainImage)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Name = "frmQ4"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Question 5"
        CType(Me.imgQ4_MainImage, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents imgQ4_MainImage As System.Windows.Forms.PictureBox
    Friend WithEvents btnQ4_OK As System.Windows.Forms.Button
    Friend WithEvents rbtnQ4_Choice0 As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnQ4_Choice3 As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnQ4_Choice2 As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnQ4_Choice1 As System.Windows.Forms.RadioButton
    Friend WithEvents lblQ4_Choice0 As System.Windows.Forms.Label
    Friend WithEvents lblQ4_Choice1 As System.Windows.Forms.Label
    Friend WithEvents lblQ4_Choice3 As System.Windows.Forms.Label
    Friend WithEvents lblQ4_Choice2 As System.Windows.Forms.Label
    Friend WithEvents btnQ4_Cancel As System.Windows.Forms.Button
    Friend WithEvents lblQ4_MainQuestion As System.Windows.Forms.Label
End Class
