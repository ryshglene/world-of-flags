﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmQuestionMenu
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnQuestionMenu_Q01 = New System.Windows.Forms.Button()
        Me.btnQuestionMenu_Q02 = New System.Windows.Forms.Button()
        Me.btnQuestionMenu_Q03 = New System.Windows.Forms.Button()
        Me.btnQuestionMenu_Q04 = New System.Windows.Forms.Button()
        Me.btnQuestionMenu_Q05 = New System.Windows.Forms.Button()
        Me.btnQuestionMenu_Q06 = New System.Windows.Forms.Button()
        Me.btnQuestionMenu_Q07 = New System.Windows.Forms.Button()
        Me.btnQuestionMenu_Q08 = New System.Windows.Forms.Button()
        Me.btnQuestionMenu_Q09 = New System.Windows.Forms.Button()
        Me.btnQuestionMenu_Q10 = New System.Windows.Forms.Button()
        Me.btnQuestionMenu_Quit = New System.Windows.Forms.Button()
        Me.btnQuestionMenu_Finish = New System.Windows.Forms.Button()
        Me.btnQuestionMenu_MainMenu = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'btnQuestionMenu_Q01
        '
        Me.btnQuestionMenu_Q01.Location = New System.Drawing.Point(20, 20)
        Me.btnQuestionMenu_Q01.Name = "btnQuestionMenu_Q01"
        Me.btnQuestionMenu_Q01.Size = New System.Drawing.Size(80, 35)
        Me.btnQuestionMenu_Q01.TabIndex = 0
        Me.btnQuestionMenu_Q01.Text = "Question 1"
        Me.btnQuestionMenu_Q01.UseVisualStyleBackColor = True
        '
        'btnQuestionMenu_Q02
        '
        Me.btnQuestionMenu_Q02.Location = New System.Drawing.Point(126, 20)
        Me.btnQuestionMenu_Q02.Name = "btnQuestionMenu_Q02"
        Me.btnQuestionMenu_Q02.Size = New System.Drawing.Size(80, 35)
        Me.btnQuestionMenu_Q02.TabIndex = 1
        Me.btnQuestionMenu_Q02.Text = "Question 2"
        Me.btnQuestionMenu_Q02.UseVisualStyleBackColor = True
        '
        'btnQuestionMenu_Q03
        '
        Me.btnQuestionMenu_Q03.Location = New System.Drawing.Point(20, 76)
        Me.btnQuestionMenu_Q03.Name = "btnQuestionMenu_Q03"
        Me.btnQuestionMenu_Q03.Size = New System.Drawing.Size(80, 35)
        Me.btnQuestionMenu_Q03.TabIndex = 2
        Me.btnQuestionMenu_Q03.Text = "Question 3"
        Me.btnQuestionMenu_Q03.UseVisualStyleBackColor = True
        '
        'btnQuestionMenu_Q04
        '
        Me.btnQuestionMenu_Q04.Location = New System.Drawing.Point(126, 76)
        Me.btnQuestionMenu_Q04.Name = "btnQuestionMenu_Q04"
        Me.btnQuestionMenu_Q04.Size = New System.Drawing.Size(80, 35)
        Me.btnQuestionMenu_Q04.TabIndex = 3
        Me.btnQuestionMenu_Q04.Text = "Question 4"
        Me.btnQuestionMenu_Q04.UseVisualStyleBackColor = True
        '
        'btnQuestionMenu_Q05
        '
        Me.btnQuestionMenu_Q05.Location = New System.Drawing.Point(20, 132)
        Me.btnQuestionMenu_Q05.Name = "btnQuestionMenu_Q05"
        Me.btnQuestionMenu_Q05.Size = New System.Drawing.Size(80, 35)
        Me.btnQuestionMenu_Q05.TabIndex = 4
        Me.btnQuestionMenu_Q05.Text = "Question 5"
        Me.btnQuestionMenu_Q05.UseVisualStyleBackColor = True
        '
        'btnQuestionMenu_Q06
        '
        Me.btnQuestionMenu_Q06.Location = New System.Drawing.Point(126, 132)
        Me.btnQuestionMenu_Q06.Name = "btnQuestionMenu_Q06"
        Me.btnQuestionMenu_Q06.Size = New System.Drawing.Size(80, 35)
        Me.btnQuestionMenu_Q06.TabIndex = 5
        Me.btnQuestionMenu_Q06.Text = "Question 6"
        Me.btnQuestionMenu_Q06.UseVisualStyleBackColor = True
        '
        'btnQuestionMenu_Q07
        '
        Me.btnQuestionMenu_Q07.Location = New System.Drawing.Point(20, 188)
        Me.btnQuestionMenu_Q07.Name = "btnQuestionMenu_Q07"
        Me.btnQuestionMenu_Q07.Size = New System.Drawing.Size(80, 35)
        Me.btnQuestionMenu_Q07.TabIndex = 6
        Me.btnQuestionMenu_Q07.Text = "Question 7"
        Me.btnQuestionMenu_Q07.UseVisualStyleBackColor = True
        '
        'btnQuestionMenu_Q08
        '
        Me.btnQuestionMenu_Q08.Location = New System.Drawing.Point(126, 188)
        Me.btnQuestionMenu_Q08.Name = "btnQuestionMenu_Q08"
        Me.btnQuestionMenu_Q08.Size = New System.Drawing.Size(80, 35)
        Me.btnQuestionMenu_Q08.TabIndex = 7
        Me.btnQuestionMenu_Q08.Text = "Question 8"
        Me.btnQuestionMenu_Q08.UseVisualStyleBackColor = True
        '
        'btnQuestionMenu_Q09
        '
        Me.btnQuestionMenu_Q09.Location = New System.Drawing.Point(20, 244)
        Me.btnQuestionMenu_Q09.Name = "btnQuestionMenu_Q09"
        Me.btnQuestionMenu_Q09.Size = New System.Drawing.Size(80, 35)
        Me.btnQuestionMenu_Q09.TabIndex = 8
        Me.btnQuestionMenu_Q09.Text = "Question 9"
        Me.btnQuestionMenu_Q09.UseVisualStyleBackColor = True
        '
        'btnQuestionMenu_Q10
        '
        Me.btnQuestionMenu_Q10.Location = New System.Drawing.Point(126, 244)
        Me.btnQuestionMenu_Q10.Name = "btnQuestionMenu_Q10"
        Me.btnQuestionMenu_Q10.Size = New System.Drawing.Size(80, 35)
        Me.btnQuestionMenu_Q10.TabIndex = 9
        Me.btnQuestionMenu_Q10.Text = "Question 10"
        Me.btnQuestionMenu_Q10.UseVisualStyleBackColor = True
        '
        'btnQuestionMenu_Quit
        '
        Me.btnQuestionMenu_Quit.Location = New System.Drawing.Point(20, 300)
        Me.btnQuestionMenu_Quit.Name = "btnQuestionMenu_Quit"
        Me.btnQuestionMenu_Quit.Size = New System.Drawing.Size(50, 23)
        Me.btnQuestionMenu_Quit.TabIndex = 10
        Me.btnQuestionMenu_Quit.Text = "Quit"
        Me.btnQuestionMenu_Quit.UseVisualStyleBackColor = True
        '
        'btnQuestionMenu_Finish
        '
        Me.btnQuestionMenu_Finish.Location = New System.Drawing.Point(156, 300)
        Me.btnQuestionMenu_Finish.Name = "btnQuestionMenu_Finish"
        Me.btnQuestionMenu_Finish.Size = New System.Drawing.Size(50, 23)
        Me.btnQuestionMenu_Finish.TabIndex = 11
        Me.btnQuestionMenu_Finish.Text = "Finish"
        Me.btnQuestionMenu_Finish.UseVisualStyleBackColor = True
        '
        'btnQuestionMenu_MainMenu
        '
        Me.btnQuestionMenu_MainMenu.Location = New System.Drawing.Point(76, 300)
        Me.btnQuestionMenu_MainMenu.Name = "btnQuestionMenu_MainMenu"
        Me.btnQuestionMenu_MainMenu.Size = New System.Drawing.Size(74, 23)
        Me.btnQuestionMenu_MainMenu.TabIndex = 12
        Me.btnQuestionMenu_MainMenu.Text = "Main Menu"
        Me.btnQuestionMenu_MainMenu.UseVisualStyleBackColor = True
        '
        'frmQuestionMenu
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(226, 343)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnQuestionMenu_MainMenu)
        Me.Controls.Add(Me.btnQuestionMenu_Finish)
        Me.Controls.Add(Me.btnQuestionMenu_Quit)
        Me.Controls.Add(Me.btnQuestionMenu_Q10)
        Me.Controls.Add(Me.btnQuestionMenu_Q09)
        Me.Controls.Add(Me.btnQuestionMenu_Q08)
        Me.Controls.Add(Me.btnQuestionMenu_Q07)
        Me.Controls.Add(Me.btnQuestionMenu_Q06)
        Me.Controls.Add(Me.btnQuestionMenu_Q05)
        Me.Controls.Add(Me.btnQuestionMenu_Q04)
        Me.Controls.Add(Me.btnQuestionMenu_Q03)
        Me.Controls.Add(Me.btnQuestionMenu_Q02)
        Me.Controls.Add(Me.btnQuestionMenu_Q01)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Name = "frmQuestionMenu"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Questions"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnQuestionMenu_Q01 As System.Windows.Forms.Button
    Friend WithEvents btnQuestionMenu_Q02 As System.Windows.Forms.Button
    Friend WithEvents btnQuestionMenu_Q03 As System.Windows.Forms.Button
    Friend WithEvents btnQuestionMenu_Q04 As System.Windows.Forms.Button
    Friend WithEvents btnQuestionMenu_Q05 As System.Windows.Forms.Button
    Friend WithEvents btnQuestionMenu_Q06 As System.Windows.Forms.Button
    Friend WithEvents btnQuestionMenu_Q07 As System.Windows.Forms.Button
    Friend WithEvents btnQuestionMenu_Q08 As System.Windows.Forms.Button
    Friend WithEvents btnQuestionMenu_Q09 As System.Windows.Forms.Button
    Friend WithEvents btnQuestionMenu_Q10 As System.Windows.Forms.Button
    Friend WithEvents btnQuestionMenu_Quit As System.Windows.Forms.Button
    Friend WithEvents btnQuestionMenu_Finish As System.Windows.Forms.Button
    Friend WithEvents btnQuestionMenu_MainMenu As System.Windows.Forms.Button
End Class
