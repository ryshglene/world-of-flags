﻿Public Class frmOptions

    Private Sub btnOptions_Back_Click(sender As System.Object, e As System.EventArgs) Handles btnOptions_OK.Click
        frmWelcomeMenu.Show()
        Me.Hide()
    End Sub
    Public quizSettingsCheatMode As Boolean = False
    Public quizSettingsDisplayHScore As Boolean = False
    Public quizSettingsAutoSaveScore As Boolean = False
    Private Sub chkOptions_CheatMode_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chkOptions_CheatMode.CheckedChanged
        If chkOptions_CheatMode.Checked = True Then
            quizSettingsCheatMode = True
        Else
            quizSettingsCheatMode = False
        End If
    End Sub

    Private Sub chkOptions_DisplayHScore_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chkOptions_DisplayHScore.CheckedChanged
        If chkOptions_DisplayHScore.Checked = True Then
            quizSettingsDisplayHScore = True
        Else
            quizSettingsDisplayHScore = False
        End If
    End Sub

    Private Sub chkOptions_AutoSaveScore_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chkOptions_AutoSaveScore.CheckedChanged
        If chkOptions_AutoSaveScore.Checked = True Then
            quizSettingsAutoSaveScore = True
        Else
            quizSettingsAutoSaveScore = False
        End If
    End Sub
End Class