﻿Public Class frmQ5

    Dim quizImage As String = frmQuestionMenu.quizImagesPath + frmQuestionMenu.quizImagesShuffled(5) + frmQuestionMenu.quizImagesExt
    Dim quizChoices() As String = {frmQuestionMenu.quizImagesShuffled(180), frmQuestionMenu.quizImagesShuffled(179), frmQuestionMenu.quizImagesShuffled(5), frmQuestionMenu.quizImagesShuffled(178)}
    Dim quizChoicesCorrectAnswer As String = quizChoices(2) ' Do not change this
    Dim quizChoicesShuffled = quizChoices.OrderBy(Function() frmQuestionMenu.randomNumberGenerator.Next).ToArray

    Private Sub frmQ5_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        imgQ5_MainImage.Image = Image.FromFile(quizImage)
        If frmOptions.quizSettingsCheatMode = True Then
            lblQ5_Choice0.Text = quizChoices(0)
            lblQ5_Choice1.Text = quizChoices(1)
            lblQ5_Choice2.Text = quizChoices(2)
            lblQ5_Choice3.Text = quizChoices(3)
            rbtnQ5_Choice2.Checked = True
        Else
            lblQ5_Choice0.Text = quizChoicesShuffled(0)
            lblQ5_Choice1.Text = quizChoicesShuffled(1)
            lblQ5_Choice2.Text = quizChoicesShuffled(2)
            lblQ5_Choice3.Text = quizChoicesShuffled(3)
        End If
    End Sub

    Private Sub btnQ5_OK_Click(sender As System.Object, e As System.EventArgs) Handles btnQ5_OK.Click
        Dim quizChoicesShuffledCorrectAnswerIndex As Integer = Array.IndexOf(quizChoicesShuffled, quizChoicesCorrectAnswer)
        If frmOptions.quizSettingsCheatMode = True Then
            quizChoicesShuffledCorrectAnswerIndex = Array.IndexOf(quizChoices, quizChoicesCorrectAnswer)
        Else
            quizChoicesShuffledCorrectAnswerIndex = Array.IndexOf(quizChoicesShuffled, quizChoicesCorrectAnswer)
        End If

        Dim quizChoicesShuffledDict As New Dictionary(Of String, Boolean)
        quizChoicesShuffledDict.Add("choice0", rbtnQ5_Choice0.Checked)
        quizChoicesShuffledDict.Add("choice1", rbtnQ5_Choice1.Checked)
        quizChoicesShuffledDict.Add("choice2", rbtnQ5_Choice2.Checked)
        quizChoicesShuffledDict.Add("choice3", rbtnQ5_Choice3.Checked)
        If quizChoicesShuffledDict("choice" & quizChoicesShuffledCorrectAnswerIndex) = True Then
            MsgBox("Correct!")
            frmQuestionMenu.quizScoreCounter = frmQuestionMenu.quizScoreCounter + 1
        Else
            MsgBox("Wrong! The correct answer is " & quizChoicesCorrectAnswer.ToString & ".")
        End If
        frmQuestionMenu.btnQuestionMenu_Q06.Enabled = False
        frmQuestionMenu.quizOKCounter = frmQuestionMenu.quizOKCounter + 1
        If frmQuestionMenu.quizOKCounter = 10 Then
            frmScore.Show()
            frmQuestionMenu.Close()
        Else
            frmQuestionMenu.Show()
        End If
        Me.Close()
    End Sub

    Private Sub btnQ5_Cancel_Click(sender As System.Object, e As System.EventArgs) Handles btnQ5_Cancel.Click
        frmQuestionMenu.Show()
        Me.Hide()
    End Sub
End Class