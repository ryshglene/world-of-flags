﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmQ2
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.imgQ2_MainImage = New System.Windows.Forms.PictureBox()
        Me.btnQ2_OK = New System.Windows.Forms.Button()
        Me.rbtnQ2_Choice0 = New System.Windows.Forms.RadioButton()
        Me.rbtnQ2_Choice3 = New System.Windows.Forms.RadioButton()
        Me.rbtnQ2_Choice2 = New System.Windows.Forms.RadioButton()
        Me.rbtnQ2_Choice1 = New System.Windows.Forms.RadioButton()
        Me.lblQ2_Choice0 = New System.Windows.Forms.Label()
        Me.lblQ2_Choice1 = New System.Windows.Forms.Label()
        Me.lblQ2_Choice3 = New System.Windows.Forms.Label()
        Me.lblQ2_Choice2 = New System.Windows.Forms.Label()
        Me.btnQ2_Cancel = New System.Windows.Forms.Button()
        Me.lblQ2_MainQuestion = New System.Windows.Forms.Label()
        CType(Me.imgQ2_MainImage, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'imgQ2_MainImage
        '
        Me.imgQ2_MainImage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.imgQ2_MainImage.Location = New System.Drawing.Point(12, 12)
        Me.imgQ2_MainImage.Name = "imgQ2_MainImage"
        Me.imgQ2_MainImage.Size = New System.Drawing.Size(240, 180)
        Me.imgQ2_MainImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.imgQ2_MainImage.TabIndex = 0
        Me.imgQ2_MainImage.TabStop = False
        '
        'btnQ2_OK
        '
        Me.btnQ2_OK.Location = New System.Drawing.Point(192, 335)
        Me.btnQ2_OK.Name = "btnQ2_OK"
        Me.btnQ2_OK.Size = New System.Drawing.Size(60, 25)
        Me.btnQ2_OK.TabIndex = 1
        Me.btnQ2_OK.Text = "OK"
        Me.btnQ2_OK.UseVisualStyleBackColor = True
        '
        'rbtnQ2_Choice0
        '
        Me.rbtnQ2_Choice0.AutoSize = True
        Me.rbtnQ2_Choice0.Font = New System.Drawing.Font("Lucida Sans Typewriter", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtnQ2_Choice0.ForeColor = System.Drawing.Color.RoyalBlue
        Me.rbtnQ2_Choice0.Location = New System.Drawing.Point(32, 219)
        Me.rbtnQ2_Choice0.Name = "rbtnQ2_Choice0"
        Me.rbtnQ2_Choice0.Size = New System.Drawing.Size(36, 22)
        Me.rbtnQ2_Choice0.TabIndex = 2
        Me.rbtnQ2_Choice0.TabStop = True
        Me.rbtnQ2_Choice0.Text = "A"
        Me.rbtnQ2_Choice0.UseVisualStyleBackColor = True
        '
        'rbtnQ2_Choice3
        '
        Me.rbtnQ2_Choice3.AccessibleDescription = ""
        Me.rbtnQ2_Choice3.AutoSize = True
        Me.rbtnQ2_Choice3.Font = New System.Drawing.Font("Lucida Sans Typewriter", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtnQ2_Choice3.ForeColor = System.Drawing.Color.RoyalBlue
        Me.rbtnQ2_Choice3.Location = New System.Drawing.Point(32, 303)
        Me.rbtnQ2_Choice3.Name = "rbtnQ2_Choice3"
        Me.rbtnQ2_Choice3.Size = New System.Drawing.Size(36, 22)
        Me.rbtnQ2_Choice3.TabIndex = 3
        Me.rbtnQ2_Choice3.TabStop = True
        Me.rbtnQ2_Choice3.Text = "D"
        Me.rbtnQ2_Choice3.UseVisualStyleBackColor = True
        '
        'rbtnQ2_Choice2
        '
        Me.rbtnQ2_Choice2.AutoSize = True
        Me.rbtnQ2_Choice2.Font = New System.Drawing.Font("Lucida Sans Typewriter", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtnQ2_Choice2.ForeColor = System.Drawing.Color.RoyalBlue
        Me.rbtnQ2_Choice2.Location = New System.Drawing.Point(32, 275)
        Me.rbtnQ2_Choice2.Name = "rbtnQ2_Choice2"
        Me.rbtnQ2_Choice2.Size = New System.Drawing.Size(36, 22)
        Me.rbtnQ2_Choice2.TabIndex = 4
        Me.rbtnQ2_Choice2.TabStop = True
        Me.rbtnQ2_Choice2.Text = "C"
        Me.rbtnQ2_Choice2.UseVisualStyleBackColor = True
        '
        'rbtnQ2_Choice1
        '
        Me.rbtnQ2_Choice1.AutoSize = True
        Me.rbtnQ2_Choice1.Font = New System.Drawing.Font("Lucida Sans Typewriter", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtnQ2_Choice1.ForeColor = System.Drawing.Color.RoyalBlue
        Me.rbtnQ2_Choice1.Location = New System.Drawing.Point(32, 247)
        Me.rbtnQ2_Choice1.Name = "rbtnQ2_Choice1"
        Me.rbtnQ2_Choice1.Size = New System.Drawing.Size(36, 22)
        Me.rbtnQ2_Choice1.TabIndex = 5
        Me.rbtnQ2_Choice1.TabStop = True
        Me.rbtnQ2_Choice1.Text = "B"
        Me.rbtnQ2_Choice1.UseVisualStyleBackColor = True
        '
        'lblQ2_Choice0
        '
        Me.lblQ2_Choice0.AutoSize = True
        Me.lblQ2_Choice0.Location = New System.Drawing.Point(91, 224)
        Me.lblQ2_Choice0.Name = "lblQ2_Choice0"
        Me.lblQ2_Choice0.Size = New System.Drawing.Size(64, 13)
        Me.lblQ2_Choice0.TabIndex = 6
        Me.lblQ2_Choice0.Text = "<country A>"
        '
        'lblQ2_Choice1
        '
        Me.lblQ2_Choice1.AutoSize = True
        Me.lblQ2_Choice1.Location = New System.Drawing.Point(90, 252)
        Me.lblQ2_Choice1.Name = "lblQ2_Choice1"
        Me.lblQ2_Choice1.Size = New System.Drawing.Size(64, 13)
        Me.lblQ2_Choice1.TabIndex = 7
        Me.lblQ2_Choice1.Text = "<country B>"
        '
        'lblQ2_Choice3
        '
        Me.lblQ2_Choice3.AutoSize = True
        Me.lblQ2_Choice3.Location = New System.Drawing.Point(90, 308)
        Me.lblQ2_Choice3.Name = "lblQ2_Choice3"
        Me.lblQ2_Choice3.Size = New System.Drawing.Size(65, 13)
        Me.lblQ2_Choice3.TabIndex = 8
        Me.lblQ2_Choice3.Text = "<country D>"
        '
        'lblQ2_Choice2
        '
        Me.lblQ2_Choice2.AutoSize = True
        Me.lblQ2_Choice2.Location = New System.Drawing.Point(90, 279)
        Me.lblQ2_Choice2.Name = "lblQ2_Choice2"
        Me.lblQ2_Choice2.Size = New System.Drawing.Size(64, 13)
        Me.lblQ2_Choice2.TabIndex = 9
        Me.lblQ2_Choice2.Text = "<country C>"
        '
        'btnQ2_Cancel
        '
        Me.btnQ2_Cancel.Location = New System.Drawing.Point(126, 335)
        Me.btnQ2_Cancel.Name = "btnQ2_Cancel"
        Me.btnQ2_Cancel.Size = New System.Drawing.Size(60, 25)
        Me.btnQ2_Cancel.TabIndex = 10
        Me.btnQ2_Cancel.Text = "Cancel"
        Me.btnQ2_Cancel.UseVisualStyleBackColor = True
        '
        'lblQ2_MainQuestion
        '
        Me.lblQ2_MainQuestion.AutoSize = True
        Me.lblQ2_MainQuestion.Font = New System.Drawing.Font("Lucida Sans", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblQ2_MainQuestion.Location = New System.Drawing.Point(17, 198)
        Me.lblQ2_MainQuestion.Name = "lblQ2_MainQuestion"
        Me.lblQ2_MainQuestion.Size = New System.Drawing.Size(229, 15)
        Me.lblQ2_MainQuestion.TabIndex = 11
        Me.lblQ2_MainQuestion.Text = "Which country has this kind of flag?"
        '
        'frmQ2
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(264, 372)
        Me.ControlBox = False
        Me.Controls.Add(Me.lblQ2_MainQuestion)
        Me.Controls.Add(Me.btnQ2_Cancel)
        Me.Controls.Add(Me.lblQ2_Choice2)
        Me.Controls.Add(Me.lblQ2_Choice3)
        Me.Controls.Add(Me.lblQ2_Choice1)
        Me.Controls.Add(Me.lblQ2_Choice0)
        Me.Controls.Add(Me.rbtnQ2_Choice1)
        Me.Controls.Add(Me.rbtnQ2_Choice2)
        Me.Controls.Add(Me.rbtnQ2_Choice3)
        Me.Controls.Add(Me.rbtnQ2_Choice0)
        Me.Controls.Add(Me.btnQ2_OK)
        Me.Controls.Add(Me.imgQ2_MainImage)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Name = "frmQ2"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Question 3"
        CType(Me.imgQ2_MainImage, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents imgQ2_MainImage As System.Windows.Forms.PictureBox
    Friend WithEvents btnQ2_OK As System.Windows.Forms.Button
    Friend WithEvents rbtnQ2_Choice0 As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnQ2_Choice3 As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnQ2_Choice2 As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnQ2_Choice1 As System.Windows.Forms.RadioButton
    Friend WithEvents lblQ2_Choice0 As System.Windows.Forms.Label
    Friend WithEvents lblQ2_Choice1 As System.Windows.Forms.Label
    Friend WithEvents lblQ2_Choice3 As System.Windows.Forms.Label
    Friend WithEvents lblQ2_Choice2 As System.Windows.Forms.Label
    Friend WithEvents btnQ2_Cancel As System.Windows.Forms.Button
    Friend WithEvents lblQ2_MainQuestion As System.Windows.Forms.Label
End Class
