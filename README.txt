
 --- [ POST-INSTALL INSTRUCTIONS ] ---

Do the following before running the program:
1. Unzip assets.zip
2. Copy the extracted `assets` directory to C:\
3. Run the program

*  The program will not work without the C:\assets directory
   because it contains the images for the quiz questions

                             -- https://gitlab.com/ryshglene
                        Copyright (c) 2018 The Lemon Project
